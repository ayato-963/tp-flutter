// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tp_flutter/infrastructure/contact.dart';
import 'package:tp_flutter/presentation/pageContact.dart';
import 'package:tp_flutter/presentation/widgets/contactCard.dart';
import 'controllers/pageListController.dart';

class PageList extends GetView<PageListController> {
  const PageList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final RxList<Contact> listContact = controller.contactList;

    return Scaffold(
      backgroundColor: Colors.blueGrey.shade100,
      appBar: AppBar(
        title: Text("Mon app",
            style: TextStyle(fontSize: 25, color: Colors.black)),
        centerTitle: true,
        backgroundColor: Colors.blueGrey.shade100,
        shape: Border(
            bottom: BorderSide(color: Colors.blueGrey.shade400, width: 1)),
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        child: Container(
            margin: new EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              children: [
                Obx(
                  () => ListView(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    children: [
                      ...List.generate(
                        listContact.length,
                        (index) => ContactCard(
                            nom: listContact[index].nom.value,
                            prenom: listContact[index].prenom.value,
                            societe: listContact[index].societe.value,
                            poste: listContact[index].poste.value,
                            mail: listContact[index].mail.value,
                            telPro: listContact[index].telPro.value,
                            telMobile: listContact[index].telMobile.value,
                            telPerso: listContact[index].telPerso.value),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                )
              ],
            )),
      ),
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.grey.shade700,
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PageContact(
                nom: "",
                prenom: "",
                societe: "",
                poste: "",
                mail: "",
                telPro: "",
                telMobile: "",
                telPerso: "",
              ),
            ),
          );
        },
        backgroundColor: Colors.blueGrey.shade300,
        child: const Icon(
          Icons.add,
        ),
      ),
    );
  }

  // @override
  // Widget contactCard(BuildContext context) {
  //   return Padding(
  //     padding: const EdgeInsets.only(top: 20.0),
  //     child: Container(
  //         decoration: BoxDecoration(
  //             border: Border.all(color: Colors.grey.shade700),
  //             color: Colors.white),
  //         child: Column(
  //           children: [
  //             Text(listContact[index].prenom.value),
  //             Text(),
  //             Text(),
  //           ],
  //         )),
  //   );
  // }
}
