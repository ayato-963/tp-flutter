// ignore_for_file: annotate_overrides, unused_label, file_names

import 'package:get/get.dart';
import 'package:tp_flutter/infrastructure/contact.dart';
import 'package:tp_flutter/presentation/widgets/contactCard.dart';

class PageListController extends GetxController {
  PageListController();

  RxList<Contact> contactList = <Contact>[
    Contact(
        nom: "jean".obs,
        prenom: "luk".obs,
        societe: "fakeenterprise".obs,
        poste: "cadre".obs,
        mail: "jean.luc1@outlook.fr".obs,
        telPro: "0782696853".obs,
        telMobile: "0782696854".obs,
        telPerso: "0782696855".obs),
    Contact(
        nom: "jean".obs,
        prenom: "luc".obs,
        societe: "fakeenterprise".obs,
        poste: "cadre".obs,
        mail: "jean.luc2@outlook.fr".obs,
        telPro: "0782696853".obs,
        telMobile: "0782696854".obs,
        telPerso: "0782696855".obs),
    Contact(
        nom: "jean".obs,
        prenom: "luc".obs,
        societe: "fakeenterprise".obs,
        poste: "cadre".obs,
        mail: "jean.luc3@outlook.fr".obs,
        telPro: "0782696853".obs,
        telMobile: "0782696854".obs,
        telPerso: "0782696855".obs),
    Contact(
        nom: "jean".obs,
        prenom: "luk".obs,
        societe: "fakeenterprise".obs,
        poste: "cadre".obs,
        mail: "jean.luc4@outlook.fr".obs,
        telPro: "0782696853".obs,
        telMobile: "0782696854".obs,
        telPerso: "0782696855".obs),
    Contact(
        nom: "jean".obs,
        prenom: "luk".obs,
        societe: "fakeenterprise".obs,
        poste: "cadre".obs,
        mail: "jean.luc5@outlook.fr".obs,
        telPro: "0782696853".obs,
        telMobile: "0782696854".obs,
        telPerso: "0782696855".obs),
    Contact(
        nom: "jean".obs,
        prenom: "luk".obs,
        societe: "fakeenterprise".obs,
        poste: "cadre".obs,
        mail: "jean.luc6@outlook.fr".obs,
        telPro: "0782696853".obs,
        telMobile: "0782696854".obs,
        telPerso: "0782696855".obs),
    Contact(
        nom: "jean".obs,
        prenom: "luk".obs,
        societe: "fakeenterprise".obs,
        poste: "cadre".obs,
        mail: "jean.luc7@outlook.fr".obs,
        telPro: "0782696853".obs,
        telMobile: "0782696854".obs,
        telPerso: "0782696855".obs),
    Contact(
        nom: "jean".obs,
        prenom: "luk".obs,
        societe: "fakeenterprise".obs,
        poste: "cadre".obs,
        mail: "jean.luc8@outlook.fr".obs,
        telPro: "0782696853".obs,
        telMobile: "0782696854".obs,
        telPerso: "0782696855".obs),
  ].obs;

  void onInit() async {
    super.onInit();
  }

//   void addContact() {}
// }
  void deleteContact(mail) {
    contactList.removeWhere((item) => item.mail == mail);
  }

  void modifyContact(List<String> listModif) {
    final RxList<Contact> listContact = contactList;

    for (var element in listContact) {
      if (element.mail.value == listModif[8]) {
        element.nom.value = listModif[0];
        element.prenom.value = listModif[1];
        element.societe.value = listModif[2];
        element.poste.value = listModif[3];
        element.telPro.value = listModif[4];
        element.telMobile.value = listModif[5];
        element.telPerso.value = listModif[6];
        element.mail.value = listModif[7];
      }
    }
  }

  void addContact(List<String> listModif) {
    final RxList<Contact> listContact = contactList;
    listContact.add(Contact(
        nom: listModif[0].obs,
        prenom: listModif[1].obs,
        societe: listModif[2].obs,
        poste: listModif[3].obs,
        mail: listModif[7].obs,
        telPro: listModif[4].obs,
        telMobile: listModif[5].obs,
        telPerso: listModif[6].obs));
  }

  affichageAppBar(String nom, String prenom) {
    if (nom != "" || prenom != "") {
      return prenom + " " + nom;
    } else {
      return "Création d'un contact";
    }
  }
}
