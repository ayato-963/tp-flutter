// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tp_flutter/infrastructure/contact.dart';
import 'package:tp_flutter/presentation/pageListe.dart';

import 'controllers/pageListController.dart';

class PageContact extends GetView<PageListController> {
  final String nom;
  final String prenom;
  final String societe;
  final String poste;
  final String mail;
  final String telPro;
  final String telMobile;
  final String telPerso;
  PageContact(
      {Key? key,
      required this.nom,
      required this.prenom,
      required this.societe,
      required this.poste,
      required this.mail,
      required this.telPro,
      required this.telMobile,
      required this.telPerso})
      : super(key: key);

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final RxList<Contact> listContact = controller.contactList;
    List<String> listModif = [];

    return Scaffold(
      backgroundColor: Colors.blueGrey.shade100,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade100,
        title: Padding(
          padding: EdgeInsets.only(left: 10),
          child: Text(controller.affichageAppBar(nom, prenom),
              style: TextStyle(color: Colors.black)),
        ),
        automaticallyImplyLeading: false,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 15),
            child: IconButton(
              icon: const Icon(Icons.check_circle_outline_rounded),
              color: Colors.black,
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  if (mail != "") {
                    controller.modifyContact(listModif);
                  } else {
                    controller.addContact(listModif);
                  }

                  listModif.clear();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => (PageList()),
                    ),
                  );
                }
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Padding(
                padding:
                    EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey.shade700),
                      color: Colors.white),
                  width: double.infinity,
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Informations",
                            style: TextStyle(fontSize: 25, color: Colors.grey)),
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                          child: Container(
                            width: double.infinity,
                            height: 40,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 6,
                                  child: Padding(
                                    padding: EdgeInsets.only(right: 10),
                                    child: TextFormField(
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      initialValue: nom,
                                      decoration: InputDecoration(
                                        hintText: 'Nom',
                                        contentPadding: EdgeInsets.only(
                                            bottom: 16, left: 10),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: const BorderSide(
                                              width: 3, color: Colors.grey),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                      ),
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Veuillez remplir ce champ';
                                        }
                                        return null;
                                      },
                                      onSaved: (String? value) {
                                        listModif.add(value.toString());
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 5,
                                  child: TextFormField(
                                    textAlignVertical: TextAlignVertical.center,
                                    initialValue: prenom,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.only(bottom: 16, left: 10),
                                      hintText: 'Prénom',
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            width: 3, color: Colors.grey),
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                    ),
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Veuillez remplir ce champ';
                                      }
                                      return null;
                                    },
                                    onSaved: (String? value) {
                                      listModif.add(value.toString());
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
                            width: double.infinity,
                            height: 40,
                            child: TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              initialValue: societe,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(bottom: 16, left: 10),
                                hintText: 'Société',
                                enabledBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 3, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Veuillez remplir ce champ';
                                }
                                return null;
                              },
                              onSaved: (String? value) {
                                listModif.add(value.toString());
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
                            width: double.infinity,
                            height: 40,
                            child: TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              initialValue: poste,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(bottom: 16, left: 10),
                                hintText: 'Poste',
                                enabledBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 3, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Veuillez remplir ce champ';
                                }
                                return null;
                              },
                              onSaved: (String? value) {
                                listModif.add(value.toString());
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey.shade700),
                      color: Colors.white),
                  width: double.infinity,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: 15, left: 15, right: 15, bottom: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Coordonnées",
                            style: TextStyle(fontSize: 25, color: Colors.grey)),
                        Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Container(
                            width: double.infinity,
                            height: 40,
                            child: TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              initialValue: telPro,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(bottom: 16, left: 10),
                                hintText: 'Téléphone professionnel',
                                enabledBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 3, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Veuillez remplir ce champ';
                                }
                                return null;
                              },
                              onSaved: (String? value) {
                                listModif.add(value.toString());
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
                            width: double.infinity,
                            height: 40,
                            child: TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              initialValue: telMobile,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(bottom: 16, left: 10),
                                hintText: 'Téléphone mobile',
                                enabledBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 3, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Veuillez remplir ce champ';
                                }
                                return null;
                              },
                              onSaved: (String? value) {
                                listModif.add(value.toString());
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
                            width: double.infinity,
                            height: 40,
                            child: TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              initialValue: telPerso,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(bottom: 16, left: 10),
                                hintText: 'Téléphone personnel',
                                enabledBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 3, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Veuillez remplir ce champ';
                                }
                                return null;
                              },
                              onSaved: (String? value) {
                                listModif.add(value.toString());
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Container(
                            width: double.infinity,
                            height: 40,
                            child: TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              initialValue: mail,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(bottom: 16, left: 10),
                                hintText: 'Mail',
                                enabledBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      width: 3, color: Colors.grey),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Veuillez remplir ce champ';
                                }
                                return null;
                              },
                              onSaved: (String? value) {
                                listModif.add(value.toString());
                                listModif.add(mail);
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
