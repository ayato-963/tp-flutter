// ignore_for_file: prefer_const_constructors, unnecessary_new, avoid_unnecessary_containers, file_names

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:tp_flutter/presentation/pageListe.dart';

import '../pageContact.dart';

class ContactCard extends PageList {
  final String nom;
  final String prenom;
  final String societe;
  final String poste;
  final String mail;
  final String telPro;
  final String telMobile;
  final String telPerso;
  const ContactCard(
      {Key? key,
      required this.nom,
      required this.prenom,
      required this.societe,
      required this.poste,
      required this.mail,
      required this.telPro,
      required this.telMobile,
      required this.telPerso})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey.shade700),
            color: Colors.white),
        child: Padding(
          padding: EdgeInsets.only(left: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Row(
                  children: [
                    Text(prenom),
                    Text(" "),
                    Text(nom),
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.only(right: 20),
                          child: PopupMenuButton<String>(
                            child: RotatedBox(
                              quarterTurns: 1,
                              child: const Icon(
                                Icons.more_vert_outlined,
                                color: Colors.grey,
                              ),
                            ),
                            onSelected: (result) {
                              if (result == 'Modifier') {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => PageContact(
                                      nom: nom,
                                      prenom: prenom,
                                      societe: societe,
                                      poste: poste,
                                      mail: mail,
                                      telPro: telPro,
                                      telMobile: telMobile,
                                      telPerso: telPerso,
                                    ),
                                  ),
                                );
                              }
                              if (result == "Supprimer") {
                                controller.deleteContact(mail);
                              }
                            },
                            itemBuilder: (BuildContext context) {
                              return [
                                const PopupMenuItem(
                                  child: Text("Supprimer"),
                                  value: "Supprimer",
                                ),
                                const PopupMenuItem(
                                  child: Text("Modifier"),
                                  value: "Modifier",
                                )
                              ];
                            },
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 3, bottom: 3),
                child: Row(
                  children: [
                    Icon(
                      Icons.local_phone_rounded,
                      color: Colors.grey.shade700,
                    ),
                    Text(telPro),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 10),
                child: Row(
                  children: [
                    Icon(
                      Icons.email_rounded,
                      color: Colors.grey.shade700,
                    ),
                    Text(mail),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
