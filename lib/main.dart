import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tp_flutter/presentation/pageContact.dart';
import 'presentation/controllers/pageListController.dart';
import 'presentation/pageListe.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(PageListController());
    return GetMaterialApp(
      title: 'Flutter Demo',
      initialRoute: "/list",
      getPages: [
        GetPage(
          name: "/list",
          page: () => PageList(),
        )
      ],
    );
  }
}
