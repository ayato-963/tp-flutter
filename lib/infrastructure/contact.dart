// ignore_for_file: file_names
import 'package:get/get.dart';

class Contact {
  RxString nom, prenom, societe, poste, mail, telPro, telMobile, telPerso;
  Contact({
    required this.nom,
    required this.prenom,
    required this.societe,
    required this.poste,
    required this.mail,
    required this.telPro,
    required this.telMobile,
    required this.telPerso,
  });
}
